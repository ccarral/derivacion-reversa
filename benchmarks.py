import timeit

setup = """
from params import from_json
from algoritmo import algoritmo, algoritmo_paralelo
from oracion import Oracion

reglas_file = open("reglas/r1.json")
text = reglas_file.read()

(oraciones, reglas, base_conocimiento) = from_json(text)

"""

N = 10

time1 = timeit.timeit(
    stmt="algoritmo(Oracion('h2'), reglas, base_conocimiento)", setup=setup, number=N
)
time2 = timeit.timeit(
    stmt="algoritmo_paralelo(Oracion('h2'), reglas, base_conocimiento)",
    setup=setup,
    number=N,
)

print(
    """Tiempo promedio de ejecución de algoritmo: {:.7f}
Tiempo promedio de ejecución de algoritmo paralelo: {:.7f}""".format(
        time1 / N, time2 / N
    )
)
