import unittest
from regla import Regla
from params import from_json
from oracion import Oracion


class TestRegla(unittest.TestCase):
    def setUp(self):
        global r1, r2

        r1 = Regla(
            condiciones=[Oracion("h1", params=(True,)), Oracion("h2", params=(True,))],
            conclusion="a",
        )
        r2 = Regla(
            condiciones=[Oracion("h1", params=(True,)), Oracion("h3", params=(False,))],
            conclusion="b",
        )

    def test_truth_values(self):
        self.assertTrue(r1)
        self.assertFalse(r2)


class TestJSON(unittest.TestCase):
    def test_from_json(self):
        json = """
        {
            "reglas":[
               {
                   "condiciones": ["h1"],
                   "conclusion": "h0"
               },
               {
                   "condiciones": ["h2","h3"],
                   "conclusion": "h1"
               },
               {
                   "condiciones": ["h4","h5"],
                   "conclusion": "h2"
               }

            ],

            "base_conocimiento":["h5","h2"]
        }
        """

        (oraciones, reglas, base_conocimiento) = from_json(json)

        self.assertEqual(len(oraciones), 6)
        self.assertEqual(len(reglas), 3)
        self.assertEqual(len(base_conocimiento), 2)

        for oracion in base_conocimiento:
            self.assertTrue(oracion)

        self.assertEqual(
            reglas[0],
            Regla(
                condiciones=[Oracion("h1", params=(False,))],
                conclusion=Oracion("h0", params=(False,)),
            ),
        )
