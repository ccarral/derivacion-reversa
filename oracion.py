from inspect import signature


class Oracion:
    def __init__(self, etq, params=(False,), expr=lambda x: x):
        if len(signature(expr).parameters) != len(params):
            raise Exception(msg="Error en el número de parámetros")
        self.etq = etq
        self.expr = expr
        self.params = params

    def __bool__(self):
        return self.expr(*self.params)

    def __eq__(self, other):
        return self.etq == other.etq

    def __str__(self):
        return "({})".format(self.etq)
