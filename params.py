from oracion import Oracion
from regla import Regla
import json


def from_json(string):
    """
    Lee reglas de la forma
    ```json
    {
        "reglas":[
            {
                "condiciones":[]
                "conclusion": "valor"
            }
        ],
        "base_conocimiento":[]
    }
    ```

    Regresa una lista de oraciones, una lista
    de reglas y una base de conocimiento

    """

    oraciones = []
    reglas = []
    base_conocimiento = []

    params_json = json.loads(string)
    bc = params_json["base_conocimiento"]

    for regla in params_json["reglas"]:
        condiciones = []
        for condicion in regla["condiciones"]:
            cond = Oracion(condicion, params=(False,))

            if cond.etq in bc and cond not in base_conocimiento:
                cond.params = (True,)
                base_conocimiento.append(cond)

            if cond not in oraciones:
                oraciones.append(cond)

            condiciones.append(cond)

        conclusion = Oracion(etq=regla["conclusion"], params=(False,))
        if conclusion not in oraciones:

            if conclusion.etq in bc and conclusion not in base_conocimiento:
                conclusion.params = (True,)
                base_conocimiento.append(conclusion)

            oraciones.append(conclusion)

        regla = Regla(condiciones, conclusion)
        reglas.append(regla)

    return (oraciones, reglas, base_conocimiento)
