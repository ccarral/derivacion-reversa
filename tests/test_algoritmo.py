import unittest
from regla import Regla
from params import from_json
from algoritmo import algoritmo, algoritmo_paralelo
from oracion import Oracion


class TestAlgoritmo(unittest.TestCase):
    def setUp(self):
        global oraciones, reglas, base_conocimiento, reglas_file
        reglas_file = open("reglas/r1.json")
        text = reglas_file.read()
        (oraciones, reglas, base_conocimiento) = from_json(text)

    def test_args(self):
        self.assertEqual(len(reglas), 9)

    def test_alg(self):
        s = Oracion("h2")
        self.assertTrue(algoritmo(s, reglas, base_conocimiento))

    def test_alg_paralelo(self):
        s = Oracion("h2")
        self.assertTrue(algoritmo_paralelo(s, reglas, base_conocimiento))

    def tearDown(self):
        reglas_file.close()
