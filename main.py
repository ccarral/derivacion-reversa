from algoritmo import algoritmo
from oracion import Oracion
from params import from_json
from argparse import ArgumentParser
import config


parser = ArgumentParser()

parser.add_argument("--semilla", required=True)
parser.add_argument("--reglas", default="reglas/r1.json")
parser.add_argument("--debug", action="store_true", default=False)

args = parser.parse_args()

if args.debug:
    config.DEBUG = True

texto_reglas = open(args.reglas).read()

(_oraciones, reglas, base_conocimiento) = from_json(texto_reglas)

semilla = Oracion(args.semilla)

algoritmo(semilla, reglas, base_conocimiento)
