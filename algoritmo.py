from oracion import Oracion
import config
import threading


def not_in_bc(oracion, bc):
    """Regresa verdadero si oracion no esá en la bc"""
    return oracion not in bc


def debug(fmt):
    if config.DEBUG:
        print("DEBUG:{}\n".format(fmt))


def algoritmo(semilla: Oracion, reglas, base_conocimiento) -> bool:
    if semilla in base_conocimiento:
        debug("Se encontró {} en la base de conocimiento".format(semilla))
        return True

    else:
        # Obtener reglas que derivan a semilla

        reglas_semilla = []

        for regla in reglas:
            if regla.conclusion == semilla:
                reglas_semilla.append(regla)

        # Si hay reglas que lo deriven
        if reglas_semilla:
            # Lambda que calcula el número de variables desconocidas de una regla
            vars_desconocidas = lambda regla: sum(
                not_in_bc(cond, base_conocimiento) for cond in regla.condiciones
            )

            # Ordenar reglas por facilidad de derivación
            # es decir, por la cantidad de variables desconocidas
            reglas_semilla.sort(key=vars_desconocidas)

            # Si hay dos reglas con el mismo número de variables desconocidas, entonces
            # ordenar por índice en lista de reglas

            for i in range(0, len(reglas_semilla) - 1):
                r1 = reglas_semilla[i]
                r2 = reglas_semilla[i + 1]
                # Si r1 y r2 tienen el mismo número de condiciones desconocidas, intercambiar
                r1_unknowns = vars_desconocidas(r1)
                r2_unknowns = vars_desconocidas(r2)

                if r1_unknowns == r2_unknowns:
                    r1_idx = reglas.index(r1)
                    r2_idx = reglas.index(r2)

                    if r1_idx < r2_idx:
                        # Hacer el swap
                        reglas_semilla[i], reglas_semilla[i + 1] = (
                            reglas_semilla[i + 1],
                            reglas_semilla[i],
                        )

            debug("Reglas que derivan a {}:".format(semilla))
            for regla in reglas_semilla:
                debug("{}".format(regla))

            # Si encontramos UNA regla verdadera que compruebe la hipótesis,
            # agregamos la oración a la base de conocimiento y regresamos verdadero
            for regla in reglas_semilla:
                debug("Analizando regla {}".format(regla))

                thread_pool = []
                results_queue = []

                hipotesis = True

                # Acumular procesos en una lista
                for cond in regla.condiciones:
                    proceso = threading.Thread(
                        target=lambda q, c, r, bc: q.append(
                            algoritmo_paralelo(c, r, bc)
                        ),
                        args=(results_queue, cond, reglas, base_conocimiento),
                    )
                    proceso.start()
                    thread_pool.append(proceso)

                # Acumular resultados
                for t in thread_pool:
                    t.join()

                for r in results_queue:
                    hipotesis = hipotesis and r

                if hipotesis is True:
                    debug("Se cumple {}".format(regla))
                    regla.conclusion.params = (True,)
                    base_conocimiento.append(regla.conclusion)
                    return True

        # NO hay reglas que deriven la hipótesis, regresar
        else:
            return False


def algoritmo_paralelo(semilla: Oracion, reglas, base_conocimiento) -> bool:
    if semilla in base_conocimiento:
        debug("Se encontró {} en la base de conocimiento".format(semilla))
        return True

    else:
        # Obtener reglas que derivan a semilla

        reglas_semilla = []

        for regla in reglas:
            if regla.conclusion == semilla:
                reglas_semilla.append(regla)

        # Si hay reglas que lo deriven
        if reglas_semilla:
            # Lambda que calcula el número de variables desconocidas de una regla
            vars_desconocidas = lambda regla: sum(
                not_in_bc(cond, base_conocimiento) for cond in regla.condiciones
            )

            # Ordenar reglas por facilidad de derivación
            # es decir, por la cantidad de variables desconocidas
            reglas_semilla.sort(key=vars_desconocidas)

            # Si hay dos reglas con el mismo número de variables desconocidas, entonces
            # ordenar por índice en lista de reglas

            for i in range(0, len(reglas_semilla) - 1):
                r1 = reglas_semilla[i]
                r2 = reglas_semilla[i + 1]
                # Si r1 y r2 tienen el mismo número de condiciones desconocidas, intercambiar
                r1_unknowns = vars_desconocidas(r1)
                r2_unknowns = vars_desconocidas(r2)

                if r1_unknowns == r2_unknowns:
                    r1_idx = reglas.index(r1)
                    r2_idx = reglas.index(r2)

                    if r1_idx < r2_idx:
                        # Hacer el swap
                        reglas_semilla[i], reglas_semilla[i + 1] = (
                            reglas_semilla[i + 1],
                            reglas_semilla[i],
                        )

            debug("Reglas que derivan a {}:".format(semilla))
            for regla in reglas_semilla:
                debug("{}".format(regla))

            # Si encontramos UNA regla verdadera que compruebe la hipótesis,
            # agregamos la oración a la base de conocimiento y regresamos verdadero
            for regla in reglas_semilla:
                debug("Analizando regla {}".format(regla))
                hipotesis = True
                for cond in regla.condiciones:
                    # Llamada recursiva
                    hipotesis = hipotesis and algoritmo(cond, reglas, base_conocimiento)
                if hipotesis is True:
                    debug("Se cumple {}".format(regla))
                    regla.conclusion.params = (True,)
                    base_conocimiento.append(regla.conclusion)
                    return True

        # NO hay reglas que deriven la hipótesis, regresar
        else:
            return False
