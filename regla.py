class Regla:
    def __init__(self, condiciones, conclusion):
        #  Lista de nodos
        self.condiciones = condiciones

        #  Nodo en el que concluye la regla
        self.conclusion = conclusion

        #  Si (p ^ q) => r
        #  entonces
        #  self.dependencias = [p,q] y
        #  self.conclusion = r

    def __bool__(self):
        for cond in self.condiciones:
            if not cond:
                return False

        return True

    def __eq__(self, other):
        for c in self.condiciones:
            if c not in other.condiciones:
                return False

        if self.conclusion != other.conclusion:
            return False

        return True

    def __str__(self):
        condiciones = "".join(
            [
                " {} &".format(cond)
                if idx < len(self.condiciones) - 1
                else " {} ".format(cond)
                for (idx, cond) in enumerate(self.condiciones)
            ]
        )

        return "{} -> {}".format(condiciones, self.conclusion)
